import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ex2 extends JFrame {
    JTextField x,y,z;
    JButton b;
    JLabel l1,l2,l3;
    ex2()
    {
        setSize(350,400);
        setLayout(null);
        setTitle("Suma numerelor");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        x = new JTextField();
        x.setBounds(100,50,100,50);
        y = new JTextField();
        y.setBounds(100,120,100,50);
        z = new JTextField();
        z.setBounds(100,200,100,50);
        z.setEnabled(false);
        b = new JButton("+");
        b.setBounds(100,270,100,30);
        add(y);add(b);add(z);add(x);
        l1 = new JLabel("x");
        l2 = new JLabel("y");
        l3 = new JLabel("Sum");
        l1.setBounds(70,50,20,50);
        l2.setBounds(70,120,20,50);
        l3.setBounds(30,200,50,50);
        add(l1);add(l2);add(l3);
        x.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    int x1 = Integer.parseInt(x.getText()), x2 = Integer.parseInt(y.getText());
                    int sum = x1 + x2;
                    z.setText(String.valueOf(sum));
                } catch (NumberFormatException e) {
                    z.setText("Introduceti numere!");
                }
            }
        });
        setVisible(true);
    }

    public static void main(String[] args) {
        new ex2();
    }
}