class L implements I {
    private int a;
    public void b(){}
    public void i(){}
}
class X {
    private L l;
    public void met(L l){}
}
interface I {
    void i();
}
class M {
    public A a;
    public B b;
    M(A a1) {
        a = a1;
        b = new B();
    }
}
class A {
}
class B {
}
